package com.pl.dlesiak.interview.rates.rates;

public enum Currency {
    //TODO Maybe use NOT_RECOGNIZED enum value in case of not recognized currency? IF yes, fix equals/hashcode of rates
    USD, EUR, CHF, GBP, AUD, CAD, HUF, JPY, CZK, DKK, NOK, SEK, XDR

}

package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NbpRateXmlParserTest {

    private static final String FILE_NAME = "file-name";
    private XmlDownloader xmlDownloader;
    private NbpRateXmlParser xmlParser = new NbpRateXmlParser();

    @BeforeEach
    void init() {
        xmlDownloader = new XmlDownloader(new StubXmlFileDownloader());
    }

    @Test
    void shouldParseNbpRateXmlFile() {
        Document document = xmlDownloader.downloadXml(FILE_NAME);

        NbpSellBuyRates sellBuyNbpRates = xmlParser.parseRates(document);

        assertEquals(LocalDate.of(2019, 1, 4), sellBuyNbpRates.getRecordDate());
        assertEquals(LocalDate.of(2019, 1, 7), sellBuyNbpRates.getPublicationDate());
        assertEquals(13, sellBuyNbpRates.getRates().size());
    }

}
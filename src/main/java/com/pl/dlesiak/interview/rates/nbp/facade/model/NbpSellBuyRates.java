package com.pl.dlesiak.interview.rates.nbp.facade.model;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

public final class NbpSellBuyRates {
    private final LocalDate recordDate;
    private final LocalDate publicationDate;
    private final Collection<SellBuyNbpCurrencyRate> rates;

    public NbpSellBuyRates(LocalDate recordDate, LocalDate publicationDate, Collection<SellBuyNbpCurrencyRate> rates) {
        this.recordDate = recordDate;
        this.publicationDate = publicationDate;
        this.rates = Set.copyOf(rates);
    }

    public LocalDate getRecordDate() {
        return recordDate;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public Collection<SellBuyNbpCurrencyRate> getRates() {
        return rates;
    }

    //TODO which date equals?
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NbpSellBuyRates that = (NbpSellBuyRates) o;
        return Objects.equals(recordDate, that.recordDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recordDate);
    }
}

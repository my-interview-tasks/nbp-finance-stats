package com.pl.dlesiak.interview.rates.nbp.facade;

enum TableType {
//        a - tabela kursów średnich walut obcych;
//        b - tabela kursów średnich walut niewymienialnych;
//        c - tabela kursów kupna i sprzedaży;
//        h - tabela kursów jednostek rozliczeniowych.

    //TODO check if this is proper english translation
    FOREIGN_CURRENCY_AVERAGE_EXCHANGE_RATE("a"),
    INCONVERTIBLE_CURRENCY_AVERAGE_EXCHANGE_RATE("b"),
    SELL_AND_BUY_EXCHANGE_RATE("c"),
    UNIT_ACCOUNT_EXCHANGE_RATE("h");

    private final String type;

    TableType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}

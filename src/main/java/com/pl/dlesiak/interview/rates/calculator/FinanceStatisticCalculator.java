package com.pl.dlesiak.interview.rates.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;

public class FinanceStatisticCalculator {
    private static final int SCALE = 3;

    public static double mean(Collection<? extends Number> data) {
        if (data == null) {
            throw new IllegalArgumentException("Data cannot be null");
        }
        double mean = data
                .stream()
                .mapToDouble(Number::doubleValue)
                .average()
                .orElse(Double.NaN);

        return roundDouble(mean);
    }

    public static double standardDeviation(Collection<? extends Number> data) {
        final double mean = mean(data);
        final double squaredValuesMean = data
                .stream()
                .map(value -> value.doubleValue() - mean)
                .map(value -> Math.pow(value, 2))
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(Double.NaN);
        double standardDeviation = Math.sqrt(squaredValuesMean);
        return roundDouble(standardDeviation);
    }

    private static double roundDouble(double value) {
        if (Double.isNaN(value)) {
            return Double.NaN;
        }
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(SCALE, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}

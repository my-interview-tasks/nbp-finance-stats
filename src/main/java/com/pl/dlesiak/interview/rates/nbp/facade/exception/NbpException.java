package com.pl.dlesiak.interview.rates.nbp.facade.exception;

public class NbpException extends RuntimeException {
    public NbpException(String message, Exception e) {
        super(message, e);
    }

    public NbpException(String message) {
        super(message);
    }
}

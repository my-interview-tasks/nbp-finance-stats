package com.pl.dlesiak.interview.rates.history;

import java.io.IOException;

public class HistoryLogWriteException extends RuntimeException {
    public HistoryLogWriteException(IOException e) {
        super(e);
    }
}

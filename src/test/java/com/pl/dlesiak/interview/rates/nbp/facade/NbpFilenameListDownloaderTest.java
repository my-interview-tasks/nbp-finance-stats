package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.cache.Cache;
import com.pl.dlesiak.interview.rates.cache.MemoryCache;
import com.pl.dlesiak.interview.rates.downloader.Downloader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NbpFilenameListDownloaderTest {

    private static final LocalDate FIRST_DATE = LocalDate.of(2013, 1, 2);
    private static final LocalDate MIDDLE_DATE = LocalDate.of(2013, 1, 7);
    private static final LocalDate LAST_DATE = LocalDate.of(2013, 1, 10);

    private NbpFilenameListDownloader nbpFilenameListDownloader;
    private Downloader downloader = new StubFilenamesDownloader();
    private Cache<Integer, NbpXmlFilenames> cache = new MemoryCache<>();

    @BeforeEach
    void init() {
        nbpFilenameListDownloader = new NbpFilenameListDownloader(cache, downloader);
    }

    @Test
    void shouldDownloadXmlFilenameList() {
        //when
        NbpXmlFilenames xmlRatesList = nbpFilenameListDownloader.getFilenameListByYear(2013);

        //then
        assertEquals(2013, xmlRatesList.getYear());

        assertEquals("c001z130102", xmlRatesList.getFilenameByDateAndType(FIRST_DATE, TableType.SELL_AND_BUY_EXCHANGE_RATE).get());
        assertEquals("c004z130107", xmlRatesList.getFilenameByDateAndType(MIDDLE_DATE, TableType.SELL_AND_BUY_EXCHANGE_RATE).get());
        assertEquals("c007z130110", xmlRatesList.getFilenameByDateAndType(LAST_DATE, TableType.SELL_AND_BUY_EXCHANGE_RATE).get());

        assertEquals("a001z130102", xmlRatesList.getFilenameByDateAndType(FIRST_DATE, TableType.FOREIGN_CURRENCY_AVERAGE_EXCHANGE_RATE).get());
        assertEquals("a004z130107", xmlRatesList.getFilenameByDateAndType(MIDDLE_DATE, TableType.FOREIGN_CURRENCY_AVERAGE_EXCHANGE_RATE).get());
        assertEquals("a007z130110", xmlRatesList.getFilenameByDateAndType(LAST_DATE, TableType.FOREIGN_CURRENCY_AVERAGE_EXCHANGE_RATE).get());
    }

}
package com.pl.dlesiak.interview.rates.downloader;

public class DownloadException extends RuntimeException {

    DownloadException(String message, Exception e) {
        super(message, e);
    }
}

package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.downloader.Downloader;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.InvalidNbpData;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.NoNbpAccess;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

class XmlDownloader {
    private final static String NBP_RATES_URL = "http://www.nbp.pl/kursy/xml/%s.xml";

    private final Downloader downloader;

    XmlDownloader(Downloader downloader) {
        this.downloader = downloader;
    }

    Document downloadXml(String filename) {
        String url = String.format(NBP_RATES_URL, filename);
        try (InputStream in = downloader.download(url)) {
            return getDocument(in);
        } catch (IOException e) {
            throw new NoNbpAccess("Cannot access NBP", e);
        } catch (SAXException | ParserConfigurationException e) {
            throw new InvalidNbpData("Error reading data ", e);
        }
    }

    private Document getDocument(InputStream is) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        return dBuilder.parse(is);
    }
}

package com.pl.dlesiak.interview.rates.calculator;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FinanceStatisticCalculatorTest {

    //TODO more tests case

    @ParameterizedTest
    @MethodSource("numbersMean")
    void shouldCalculateMean(List<Double> numbers, double result) {
        assertEquals(result, FinanceStatisticCalculator.mean(numbers));
    }

    @ParameterizedTest
    @MethodSource("numbersStandardDeviation")
    void shouldCalculateStandardDeviation(List<Double> numbers, double result) {
        assertEquals(FinanceStatisticCalculator.standardDeviation(numbers), result);
    }

    static Stream<Arguments> numbersMean() {
        return Stream.of(
                Arguments.of(Arrays.asList(12d, 13d, 15d, 16d, 24d, 15d, 22d, 10d, 9d, 13d, 13d, 18d, 16d, 25d, 23d, 24d), 16.750),
                Arguments.of(Collections.singletonList(3d), 3),
                Arguments.of(Arrays.asList(3d, 6.5d), 4.75),
                Arguments.of(Collections.singletonList(Double.NaN), Double.NaN),
                Arguments.of(Collections.singletonList(1), 1),
                Arguments.of(Collections.emptyList(), Double.NaN)
        );
    }

    static Stream<Arguments> numbersStandardDeviation() {
        return Stream.of(
                Arguments.of(Arrays.asList(9d, 2d, 5d, 4d, 12d, 7d, 8d, 11d, 9d, 3d, 7d, 4d, 12d, 5d, 4d, 10d, 9d, 6d, 9d, 4d), 2.983),
                Arguments.of(Collections.emptyList(), Double.NaN)
        );
    }


}
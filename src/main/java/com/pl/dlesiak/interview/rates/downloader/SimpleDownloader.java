package com.pl.dlesiak.interview.rates.downloader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;


public class SimpleDownloader implements Downloader {

    private static final Logger LOG = LogManager.getLogger(SimpleDownloader.class);
    private static final int LIMITED_ACCESS = 429;

    @Override
    public InputStream download(String url) throws IOException {
        Objects.requireNonNull(url);

        LOG.debug("Downloading " + url);
        while (true) {
            HttpURLConnection connection = null;
            try {
                URL u = new URL(url);
                connection = (HttpURLConnection) u.openConnection();
                return u.openStream();
            } catch (IOException e) {

                //TODO When used in multithreaded environment, throws HttpError 429 (NBP limits access to website).
                // Naive solution: sleep thread when 429 error occurs.
                //TODO Implement slow down/retry/proxy strategy when 429 error is thrown
                // or use queue to schedule requests
                if (connection != null && LIMITED_ACCESS == connection.getResponseCode()) {
                    LOG.warn("Too many requests! HttpStatus = 429 Url = " + url);
                    try {
                        LOG.warn("Sleeping thread for 1 second");
                        connection.disconnect();
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        throw new DownloadException("Something went wrong", ex);
                    }
                } else {
                    throw new DownloadException("Something went wrong", e);
                }
            }
            LOG.debug("Downloaded " + url);
        }
    }


}

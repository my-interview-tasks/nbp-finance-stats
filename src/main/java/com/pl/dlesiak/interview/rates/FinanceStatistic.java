package com.pl.dlesiak.interview.rates;

import com.pl.dlesiak.interview.rates.rates.Currency;

import java.time.LocalDate;

public final class FinanceStatistic {

    private final double sellRatesStandardDeviation;
    private final double buyingRatesMean;
    private final LocalDate start;
    private final LocalDate end;
    private final Currency currency;

    public FinanceStatistic(double sellingRatesStandardDeviation, double buyingRatesMean, LocalDate start, LocalDate end, Currency currency) {
        this.sellRatesStandardDeviation = sellingRatesStandardDeviation;
        this.buyingRatesMean = buyingRatesMean;
        this.start = start;
        this.end = end;
        this.currency = currency;
    }

    public double getBuyingRatesMean() {
        return buyingRatesMean;
    }

    public double getSellingRatesStandardDeviation() {
        return sellRatesStandardDeviation;
    }

    public LocalDate startDate() {
        return start;
    }

    public LocalDate endDate() {
        return end;
    }

    public Currency currency() {
        return currency;
    }

}

package com.pl.dlesiak.interview.rates.nbp.facade.exception;

public class NoNbpAccess extends NbpException {
    public NoNbpAccess(String message, Exception e) {
        super(message, e);
    }
}

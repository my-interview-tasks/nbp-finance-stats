package com.pl.dlesiak.interview.rates.history;

import com.pl.dlesiak.interview.rates.FinanceStatistic;

import java.io.Closeable;

public interface HistoryLog extends Closeable {

    void log(FinanceStatistic financeStatistic);


}

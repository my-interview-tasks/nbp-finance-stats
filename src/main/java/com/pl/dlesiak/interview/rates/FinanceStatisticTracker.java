package com.pl.dlesiak.interview.rates;

import com.pl.dlesiak.interview.rates.calculator.FinanceStatisticCalculator;
import com.pl.dlesiak.interview.rates.history.HistoryLog;
import com.pl.dlesiak.interview.rates.rates.Currency;
import com.pl.dlesiak.interview.rates.rates.Rate;
import com.pl.dlesiak.interview.rates.rates.RatesHistory;

import java.time.LocalDate;
import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FinanceStatisticTracker {
    private final RatesHistory ratesHistory;
    private final HistoryLog historyLog;

    public FinanceStatisticTracker(RatesHistory ratesHistory, HistoryLog historyLog) {
        this.ratesHistory = ratesHistory;
        this.historyLog = historyLog;
    }

    public FinanceStatistic getFinanceStatistic(LocalDate start, LocalDate end, Currency currency) {
        Collection<Rate> rates = ratesHistory.getRates(start, end, currency);

        double sellingRatesStandardDeviation = calculateSellingRatesStandardDeviation(rates);
        double buyingRatesMean = calculateBuyingRatesMean(rates);

        FinanceStatistic financeStatistic = new FinanceStatistic(sellingRatesStandardDeviation, buyingRatesMean, start, end, currency);
        historyLog.log(financeStatistic);
        return financeStatistic;
    }

    private double calculateSellingRatesStandardDeviation(Collection<Rate> rates) {
        return calculate(rates, Rate::getSellingRate, FinanceStatisticCalculator::standardDeviation);
    }

    private double calculateBuyingRatesMean(Collection<Rate> rates) {
        return calculate(rates, Rate::getBuyingRate, FinanceStatisticCalculator::mean);
    }

    private Double calculate(Collection<Rate> data, Function<Rate, Double> mapper, Function<Collection<Double>, Double> action) {
        Collection<Double> values = data
                .stream()
                .mapToDouble(mapper::apply)
                .boxed()
                .collect(Collectors.toList());
        return action.apply(values);
    }

}

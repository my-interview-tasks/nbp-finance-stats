package com.pl.dlesiak.interview.rates.nbp;

import com.pl.dlesiak.interview.rates.nbp.facade.NbpApi;
import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import com.pl.dlesiak.interview.rates.nbp.facade.model.SellBuyNbpCurrencyRate;
import com.pl.dlesiak.interview.rates.rates.Currency;
import com.pl.dlesiak.interview.rates.rates.Rate;
import com.pl.dlesiak.interview.rates.rates.RatesHistory;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class NbpRatesHistory implements RatesHistory {

    private final NbpApi nbpApi;

    public NbpRatesHistory(NbpApi nbpApi) {
        this.nbpApi = nbpApi;
    }

    /**
     * Rates history from NBP.
     * Some dates can be ignored by NBP, for example, sundays and saturdays does not have rates.
     * Last date or days in future may not have been included when NBP has not public data yet
     * If no currencies specified returns include all currencies
     * In concurrent environment, duplicate requests to nbp can occurs.
     *
     * @param start      inclusive
     * @param end        inclusive
     * @param currencies
     * @return rates from specified period of time for given currencies.
     */
    @Override
    public Collection<Rate> getRates(LocalDate start, LocalDate end, Currency... currencies) {
        return nbpApi
                .getSellAndBuyRates(start, end)
                .stream()
                .map(this::toRates)
                .flatMap(Collection::stream)
                .filter(rate -> byCurrency(rate, currencies))
                .collect(Collectors.toList());
    }

    private List<Rate> toRates(NbpSellBuyRates rates) {
        LocalDate publicationDate = rates.getPublicationDate();
        return rates.getRates()
                .stream()
                .map(nbpRate -> toRate(nbpRate, publicationDate))
                .collect(Collectors.toList());
    }

    private Rate toRate(SellBuyNbpCurrencyRate rate, LocalDate date) {
        return new Rate(
                date,
                rate.getCurrency(),
                rate.getBuyRate(),
                rate.getSellRate()
        );
    }

    private boolean byCurrency(Rate rate, Currency... currencies) {
        if (currencies.length == 0) {
            return true;
        }
        return Arrays.stream(currencies)
                .anyMatch(currency -> currency == rate.getCurrency());
    }
}

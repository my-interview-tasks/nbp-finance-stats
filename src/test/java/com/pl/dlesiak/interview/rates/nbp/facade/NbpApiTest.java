package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.cache.MemoryCache;
import com.pl.dlesiak.interview.rates.downloader.Downloader;
import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class NbpApiTest {

    private static final String YEAR_2013 = "2013";
    private static final String YEAR_2014 = "2014";
    private static final String FILENAME_LIST_FILE_PATH = "src/test/resources/nbpfacade/%s/dir%s.txt";
    private static final String FILENAME_LIST_URL = "http://www.nbp.pl/kursy/xml/dir%s.txt";
    private static final String XML_RATE_FILE_PATH = "src/test/resources/nbpfacade/%s/%s.xml";
    private static final String XML_RATE_URL = "http://www.nbp.pl/kursy/xml/%s.xml";
    private final Downloader downloader = mock(Downloader.class);
    private final NbpApi nbpApi = new NbpApi(downloader, new MemoryCache<>(), new MemoryCache<>());

    @Test
    void shouldThrowErrorWhenStartDateIsAfterEndDate() {
        LocalDate startDay = LocalDate.now();
        LocalDate endDate = LocalDate.now().minusDays(1);

        //expect
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> nbpApi.getSellAndBuyRates(startDay, endDate));
        assertEquals("End date cannot be before start date", exception.getMessage());
    }

    @Test
    void shouldReturnRatesWhenStartAndEndDateAreEqual() throws IOException {
        //given
        LocalDate date = LocalDate.of(2013, 12, 30);

        returnXmlFilenameListForYear(YEAR_2013);
        returnXmlFileRate(YEAR_2013, "c250z131230");

        //when
        Collection<NbpSellBuyRates> sellAndBuyRates = nbpApi.getSellAndBuyRates(date, date);

        //then
        assertEquals(1, sellAndBuyRates.size());

        NbpSellBuyRates actualRate = sellAndBuyRates.iterator().next();
        assertEquals(date, actualRate.getPublicationDate());
        assertEquals(LocalDate.of(2013, 12, 27), actualRate.getRecordDate());
        assertEquals(13, actualRate.getRates().size());
        //todo assert currency collection

        verify(downloader, times(1)).download(contains(".xml"));
    }

    @Test
    void shouldReturnNoRatesForDateWithoutRates() {

    }

    @Test
    void shouldReturnRatesForDifferentYears() throws IOException {
        //given
        LocalDate start = LocalDate.of(2013, 12, 30);
        LocalDate end = LocalDate.of(2014, 1, 3);

        returnXmlFilenameListForYear(YEAR_2013);
        returnXmlFileRate(YEAR_2013, "c250z131230");
        returnXmlFileRate(YEAR_2013, "c251z131231");

        returnXmlFilenameListForYear(YEAR_2014);
        returnXmlFileRate(YEAR_2014, "c001z140102");
        returnXmlFileRate(YEAR_2014, "c002z140103");

        //when
        Collection<NbpSellBuyRates> sellAndBuyRates = nbpApi.getSellAndBuyRates(start, end);

        //then
        assertEquals(4, sellAndBuyRates.size());
        //todo assert currency collection
    }

    private void returnXmlFileRate(String year, String filename) throws IOException {
        String path = String.format(XML_RATE_FILE_PATH, year, filename);
        FileInputStream fileInputStream = new FileInputStream(path);

        String url = String.format(XML_RATE_URL, filename);
        when(downloader.download(url))
                .thenReturn(fileInputStream);
    }

    private void returnXmlFilenameListForYear(String year) throws IOException {
        String path = String.format(FILENAME_LIST_FILE_PATH, year, year);
        FileInputStream fileInputStream = new FileInputStream(path);
        String url = String.format(FILENAME_LIST_URL, year);
        when(downloader.download(url))
                .thenReturn(fileInputStream);
    }


}
package com.pl.dlesiak.interview.rates.cache;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MemoryCache<KEY, VALUE> implements Cache<KEY, VALUE> {
    private Map<KEY, VALUE> cache = new ConcurrentHashMap<>();

    @Override
    public VALUE put(KEY key, VALUE value) {
        return this.cache.put(key, value);
    }

    @Override
    public VALUE get(KEY key) {
        return this.cache.get(key);
    }

}

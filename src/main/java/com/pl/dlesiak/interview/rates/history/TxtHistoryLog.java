package com.pl.dlesiak.interview.rates.history;

import com.pl.dlesiak.interview.rates.FinanceStatistic;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.format.DateTimeFormatter;

public class TxtHistoryLog implements HistoryLog {
    private final static String LOG_MSG_TEMPLATE = "PERIOD: %s - %s | CURRENCY: %s | BUYING RATES MEAN: %s | SELLING RATES STANDARD DEVIATION: %s";
    private final PrintWriter printWriter;

    public TxtHistoryLog(String filePath) {
        try {
            java.io.FileWriter fw = new java.io.FileWriter(filePath, true);
            printWriter = new PrintWriter(fw, true);
        } catch (IOException e) {
            throw new HistoryLogWriteException(e);
        }
    }

    @Override
    public void log(FinanceStatistic financeStatistic) {
        String record = prepareLogMessage(financeStatistic);
        printWriter.println(record);

        //todo what if error? ignore or throw?
        //if(printWriter.checkError()){
        //....
        //}
    }

    private String prepareLogMessage(FinanceStatistic fs) {
        return String.format(
                LOG_MSG_TEMPLATE,
                fs.startDate().format(DateTimeFormatter.BASIC_ISO_DATE),
                fs.endDate().format(DateTimeFormatter.BASIC_ISO_DATE),
                fs.currency(),
                fs.getBuyingRatesMean(),
                fs.getSellingRatesStandardDeviation());
    }

    @Override
    public void close() throws IOException {
        printWriter.close();
    }
}

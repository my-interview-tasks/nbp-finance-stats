package com.pl.dlesiak.interview.rates.nbp;

import com.pl.dlesiak.interview.rates.nbp.facade.NbpApi;
import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import com.pl.dlesiak.interview.rates.nbp.facade.model.SellBuyNbpCurrencyRate;
import com.pl.dlesiak.interview.rates.rates.Currency;
import com.pl.dlesiak.interview.rates.rates.Rate;
import com.pl.dlesiak.interview.rates.rates.RatesHistory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;

import static com.pl.dlesiak.interview.rates.rates.Currency.EUR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.Every.everyItem;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class NbpRatesHistoryTest {

    private RatesHistory ratesProvider;
    private NbpApi nbpApi = mock(NbpApi.class);

    @BeforeEach
    void init() {
        ratesProvider = new NbpRatesHistory(nbpApi);
    }

    @Test
    void shouldThrowErrorIfEndDateIsBeforeStartDate() {

    }

    @Test
    void startDateShouldBeIncluded() {

    }

    @Test
    void endDateShouldBeIncluded() {

    }

    @Test
    void shouldReturnAllRatesWhenCurrencyIsNotSpecified() {
        //given
        LocalDate start = LocalDate.now().minusDays(6);
        LocalDate end = LocalDate.now();
        returnNbpRatesForSpecifiedDates(start, end);

        //when
        Collection<Rate> rates = ratesProvider.getRates(start, end);

        //then
        assertEquals(12, rates.size());
    }

    @Test
    void shouldReturnRatesForUsdCurrency() {
        //given
        LocalDate start = LocalDate.now().minusDays(6);
        LocalDate end = LocalDate.now();
        returnNbpRatesForSpecifiedDates(start, end);

        //when
        Collection<Rate> rates = ratesProvider.getRates(start, end, Currency.USD);

        //then
        assertEquals(4, rates.size());
        assertThat(rates, everyItem(hasProperty("currency", equalTo(Currency.USD))));
    }

    @Test
    void shouldReturnRatesForUsdAndEurCurrency() {
        //given
        LocalDate start = LocalDate.now().minusDays(6);
        LocalDate end = LocalDate.now();
        returnNbpRatesForSpecifiedDates(start, end);

        //when
        Collection<Rate> rates = ratesProvider.getRates(start, end, Currency.USD, EUR);

        //then
        assertEquals(8, rates.size());
        assertThat(rates, everyItem(hasProperty("currency", anyOf(equalTo(Currency.USD), equalTo(EUR)))));

    }

    private Collection<NbpSellBuyRates> nbpRates() {
        return List.of(
                ratesXDaysBefore(6),
                ratesXDaysBefore(5),
                ratesXDaysBefore(4),
                ratesXDaysBefore(3)
        );
    }

    private NbpSellBuyRates ratesXDaysBefore(int x) {
        LocalDate date = LocalDate.now().minusDays(x);

        List<SellBuyNbpCurrencyRate> nbpRates = List.of(
                new SellBuyNbpCurrencyRate("dollar", Currency.USD, 1.2, 3.2),
                new SellBuyNbpCurrencyRate("gbp", Currency.GBP, 1.2, 3.2),
                new SellBuyNbpCurrencyRate("eur", EUR, 1.2, 3.2)
        );
        return new NbpSellBuyRates(date, date, nbpRates);
    }

    private void returnNbpRatesForSpecifiedDates(LocalDate start, LocalDate end) {
        when(nbpApi.getSellAndBuyRates(start, end))
                .thenReturn(nbpRates());
    }


}
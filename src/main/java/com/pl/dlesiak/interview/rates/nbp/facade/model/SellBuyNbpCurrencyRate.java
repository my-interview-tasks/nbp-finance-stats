package com.pl.dlesiak.interview.rates.nbp.facade.model;

import com.pl.dlesiak.interview.rates.rates.Currency;

import java.util.Objects;

public final class SellBuyNbpCurrencyRate {
    private final String currencyName;
    private final Currency currency;
    private final double buyRate;
    private final double sellRate;

    public SellBuyNbpCurrencyRate(String currencyName, Currency currency, double buyRate, double sellRate) {
        this.currencyName = currencyName;
        this.currency = currency;
        this.buyRate = buyRate;
        this.sellRate = sellRate;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public Currency getCurrency() {
        return currency;
    }

    public double getBuyRate() {
        return buyRate;
    }

    public double getSellRate() {
        return sellRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SellBuyNbpCurrencyRate that = (SellBuyNbpCurrencyRate) o;
        return currency == that.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency);
    }
}

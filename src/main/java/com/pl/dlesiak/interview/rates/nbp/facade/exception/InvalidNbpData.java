package com.pl.dlesiak.interview.rates.nbp.facade.exception;

public class InvalidNbpData extends NbpException {

    public InvalidNbpData(String message, Exception e) {
        super(message, e);
    }

    public InvalidNbpData(String message) {
        super(message);
    }
}

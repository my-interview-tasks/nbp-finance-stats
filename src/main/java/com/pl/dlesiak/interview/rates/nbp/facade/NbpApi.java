package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.cache.Cache;
import com.pl.dlesiak.interview.rates.cache.MemoryCache;
import com.pl.dlesiak.interview.rates.downloader.Downloader;
import com.pl.dlesiak.interview.rates.downloader.SimpleDownloader;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.InvalidNbpData;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.NoNbpAccess;
import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;

import static com.pl.dlesiak.interview.rates.nbp.facade.TableType.SELL_AND_BUY_EXCHANGE_RATE;

public class NbpApi {

    private static final Logger LOG = LogManager.getLogger(NbpApi.class);

    private final NbpFilenameListDownloader filenameDownloader;
    private final XmlDownloader xmlDownloader;
    private final NbpRateXmlParser nbpRatesParser;
    private final Cache<LocalDate, NbpSellBuyRates> cache;

    NbpApi(Downloader downloader, Cache<Integer, NbpXmlFilenames> filenamesCache, Cache<LocalDate, NbpSellBuyRates> nbpSellBuyRatesCache) {
        this.filenameDownloader = new NbpFilenameListDownloader(filenamesCache, downloader);
        this.xmlDownloader = new XmlDownloader(downloader);
        this.nbpRatesParser = new NbpRateXmlParser();
        this.cache = nbpSellBuyRatesCache;
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * @param start date from rates are read. Date is inclusive
     * @param end   date to rates are read. Date is inclusive
     * @return Nbp sell and buy rates for all available currencies
     * @throws IllegalArgumentException when invalid arguments
     * @throws InvalidNbpData           when data returned from nbp is corrupted or invalid
     * @throws NoNbpAccess              when cannot access nbp server
     */
    //TODO in multithreaded environment, connection to same file can be duplicated.
    //TODO use thread lock like in concurrentmap or asynchronus
    public Collection<NbpSellBuyRates> getSellAndBuyRates(LocalDate start, LocalDate end) {
        Objects.requireNonNull(start);
        Objects.requireNonNull(end);

        if (end.isBefore(start)) {
            throw new IllegalArgumentException("End date cannot be before start date");
        }

        ArrayList<NbpSellBuyRates> foundRates = new ArrayList<>();
        LocalDate currentDate = start;

        while (!currentDate.isAfter(end)) {
            LocalDate finalDate = currentDate;

            getFromCache(currentDate)
                    .or(() -> downloadRates(finalDate))
                    .ifPresent(foundRates::add);

            currentDate = currentDate.plusDays(1);
        }

        return foundRates;
    }

    private Optional<NbpSellBuyRates> getFromCache(LocalDate currentDate) {
        NbpSellBuyRates result = cache.get(currentDate);
        if (result != null) {
            LOG.debug("NbpSellBuyRates obtained from cache: " + currentDate);
        }
        return Optional.ofNullable(result);
    }

    private Optional<NbpSellBuyRates> downloadRates(LocalDate currentDate) {
        LOG.debug("No NbpSellBuyRates in cache: " + currentDate + ".\nStart downloading...");

        Optional<NbpSellBuyRates> nbpRatesOptional = filenameDownloader
                .getFilenameListByYear(currentDate.getYear())
                .getFilenameByDateAndType(currentDate, SELL_AND_BUY_EXCHANGE_RATE)
                .map(xmlDownloader::downloadXml)
                .map(nbpRatesParser::parseRates);

        nbpRatesOptional
                .ifPresent(sellBuyNbpRate -> cache.put(currentDate, sellBuyNbpRate));

        return nbpRatesOptional;
    }


    public static class Builder {
        private static final Cache<LocalDate, NbpSellBuyRates> NO_XML_CACHE = new Cache<>() {

            @Override
            public NbpSellBuyRates put(LocalDate localDate, NbpSellBuyRates nbpSellBuyRates) {

                return null;
            }

            @Override
            public NbpSellBuyRates get(LocalDate localDate) {
                return null;
            }
        };

        private static final Cache<Integer, NbpXmlFilenames> NO_FILENAMES_CACHE = new Cache<>() {

            @Override
            public NbpXmlFilenames put(Integer integer, NbpXmlFilenames nbpXmlFilenames) {

                return null;
            }

            @Override
            public NbpXmlFilenames get(Integer integer) {
                return null;
            }
        };

        private Downloader downloader;
        private boolean noRatesCache = false;
        private boolean noFilenameCache = false;

        public Builder downloader(Downloader downloader) {
            this.downloader = downloader;
            return this;
        }

        public Builder noNbpRatesCache() {
            this.noRatesCache = true;
            return this;
        }

        public Builder noFilenamesCache() {
            this.noFilenameCache = true;
            return this;
        }

        public NbpApi build() {
            Cache<LocalDate, NbpSellBuyRates> nbpSellBuyRatesCache;
            if (noRatesCache) {
                nbpSellBuyRatesCache = NO_XML_CACHE;
            } else {
                nbpSellBuyRatesCache = new MemoryCache<>();
            }

            Cache<Integer, NbpXmlFilenames> filenamesCache;
            if (noFilenameCache) {
                filenamesCache = NO_FILENAMES_CACHE;
            } else {
                filenamesCache = new MemoryCache<>();
            }

            if (downloader == null) {
                downloader = new SimpleDownloader();
            }
            return new NbpApi(downloader, filenamesCache, nbpSellBuyRatesCache);
        }

    }

}

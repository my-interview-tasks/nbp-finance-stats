package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.cache.Cache;
import com.pl.dlesiak.interview.rates.downloader.Downloader;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.NoNbpAccess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

class NbpFilenameListDownloader {

    private static final Logger LOG = LogManager.getLogger(NbpFilenameListDownloader.class);

    private static final String BOM_MARKER = "\uFEFF";
    private final static String NBP_DIRECTORY_URL = "http://www.nbp.pl/kursy/xml/dir%s.txt";

    private final Cache<Integer, NbpXmlFilenames> nbpXmlFilenamesCache;
    private final Downloader downloader;

    NbpFilenameListDownloader(Cache<Integer, NbpXmlFilenames> nbpXmlFilenamesCache, Downloader downloader) {
        this.nbpXmlFilenamesCache = nbpXmlFilenamesCache;
        this.downloader = downloader;
    }

    NbpXmlFilenames getFilenameListByYear(int year) {
        NbpXmlFilenames filenameList = nbpXmlFilenamesCache.get(year);

        if (filenameList == null) {
            LOG.debug("No FilenameList in cache: " + year + ".\nStart downloading...");
            filenameList = downloadFilenames(year);
            nbpXmlFilenamesCache.put(year, filenameList);
        } else {
            LOG.debug("FilenameList obtained from cache: " + year);
        }

        return filenameList;
    }

    private NbpXmlFilenames downloadFilenames(Integer year) {
        String url = createUrl(year);

        List<String> content;
        try (InputStream in = downloader.download(url)) {
            content = getContentAsString(in);
            removeBOM(content);
        } catch (IOException e) {
            throw new NoNbpAccess("Cannot access NBP ", e);
        }
        return new NbpXmlFilenames(year, content);
    }

    private String createUrl(Integer year) {
        String url = String.format(NBP_DIRECTORY_URL, "");

        if (isNotCurrentYear(year))
            return String.format(NBP_DIRECTORY_URL, year);

        return url;
    }

    private boolean isNotCurrentYear(int year) {
        return year != LocalDate.now().getYear();
    }

    private List<String> getContentAsString(InputStream in) {
        //todo investigate
        return new BufferedReader(new InputStreamReader(in))
                .lines()
                .collect(Collectors.toList());
    }

    private void removeBOM(List<String> content) {
        //TODO can use dropWhile/takeWhile from java 10?
        if (!content.isEmpty()) {
            String first = content.get(0);
            if (first.startsWith(BOM_MARKER)) {
                content.set(0, first.substring(1));
            }
        }
    }

}

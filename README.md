### Nbp finance statistic program

#### How to run:
mvn clean test exec:java -Dexec.mainClass=com.pl.dlesiak.interview.Main -Dexec.args=data.txt

input(currency, startdate, enddate): EUR 2013-01-28 2013-01-31

#### ToDo 
* Application is thread safe but in multithreading environment, duplicate requests to server can occurs. Get rid of possible duplicate requests (by synchronizing or queue requests etc.)
* Nbp server can return 429 error if too many requests are send. For now, naive solution is implemented, sleep thread for 1 sec. Implement retry/slow down policy.
* Use aspects for logging
* More test coverage


#### Task description:
* Write console program that downloads currency rates from NBP bank and calculates statistic.
* Application should download buy rate and sell rate for USD, EUR, CHF and GBP.
* User input is currency, start date and end date (dates are inclusive)
* Result of program should be: buying rates average and selling rates standard deviation
* Data can be obtained from http://www.nbp.pl/home.aspx?f=/kursy/instrukcja_pobierania_kursow_walut.html
* Do not use NBP Web API
* Example of xml file http://www.nbp.pl/kursy/xml/c073z070413.xml
* Do not use any tools like JAXB. You can read xml via org.w3c.dom
* Do not use any http library

##### Example 
* Input:  EUR 2013-01-28 2013-01-31
*  Output:
EUR
average: 4,151
standard deviation: 0,012






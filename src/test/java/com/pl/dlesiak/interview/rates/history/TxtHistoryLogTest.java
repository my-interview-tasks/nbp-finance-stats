package com.pl.dlesiak.interview.rates.history;

import com.pl.dlesiak.interview.rates.FinanceStatistic;
import com.pl.dlesiak.interview.rates.rates.Currency;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TxtHistoryLogTest {

    private HistoryLog log;

    @BeforeEach
    void createFile() {
        log = new TxtHistoryLog("data.txt");
    }

    @AfterEach
    void deleteFile() {
        new File("data.txt").delete();
    }

    @Test
    void shouldSaveLog() throws Exception {
        //given
        FinanceStatistic financeStatistic = fianceStatisticEur();

        //when
        log.log(financeStatistic);

        //then
        List<String> lines = readLines();
        assertEquals(1, lines.size());
        assertEquals("PERIOD: 20150101 - 20150110 | CURRENCY: EUR | BUYING RATES MEAN: 241.3 | SELLING RATES STANDARD DEVIATION: 234.4", lines.get(0));
    }

    @Test
    void shouldAppendNewLogsToFile() throws Exception {
        //given
        FinanceStatistic financeStatistic = fianceStatisticEur();
        FinanceStatistic financeStatistic2 = fianceStatisticEur();

        //when
        log.log(financeStatistic);
        log.log(financeStatistic2);

        //then
        List<String> lines = readLines();
        assertEquals(2, lines.size());
        assertEquals("PERIOD: 20150101 - 20150110 | CURRENCY: EUR | BUYING RATES MEAN: 241.3 | SELLING RATES STANDARD DEVIATION: 234.4", lines.get(0));
        assertEquals("PERIOD: 20150101 - 20150110 | CURRENCY: EUR | BUYING RATES MEAN: 241.3 | SELLING RATES STANDARD DEVIATION: 234.4", lines.get(1));
    }


    private List<String> readLines() throws FileNotFoundException {
        InputStream in = new FileInputStream(new File("data.txt"));
        return new BufferedReader(new InputStreamReader(in))
                .lines()
                .collect(Collectors.toList());
    }

    private FinanceStatistic fianceStatisticEur() {
        return new FinanceStatistic(234.4, 241.3, LocalDate.parse("2015-01-01"), LocalDate.parse("2015-01-10"), Currency.EUR);
    }


}
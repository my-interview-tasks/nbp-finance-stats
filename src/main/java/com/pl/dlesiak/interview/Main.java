package com.pl.dlesiak.interview;

import com.pl.dlesiak.interview.rates.FinanceStatistic;
import com.pl.dlesiak.interview.rates.FinanceStatisticTracker;
import com.pl.dlesiak.interview.rates.history.HistoryLogWriteException;
import com.pl.dlesiak.interview.rates.history.TxtHistoryLog;
import com.pl.dlesiak.interview.rates.nbp.NbpRatesHistory;
import com.pl.dlesiak.interview.rates.nbp.facade.NbpApi;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.InvalidNbpData;
import com.pl.dlesiak.interview.rates.nbp.facade.exception.NoNbpAccess;
import com.pl.dlesiak.interview.rates.rates.Currency;
import com.pl.dlesiak.interview.rates.rates.RatesHistory;

import java.time.LocalDate;
import java.util.Scanner;

public class Main {

    private static final String DEFAULT_PATH = "history.txt";
    private static final Scanner SCANNER = new Scanner(System.in);

    //TODO MVC pattern
    //TODO handle closing resources at exiting application
    public static void main(String[] a) {

        final String logFilePath = getFilePathForHistoryLog(a);

        final NbpApi nbpApi = NbpApi.builder().build();
        final RatesHistory nbpRatesHistory = new NbpRatesHistory(nbpApi);
        final TxtHistoryLog historyLog = new TxtHistoryLog(logFilePath);
        final FinanceStatisticTracker tracker = new FinanceStatisticTracker(nbpRatesHistory, historyLog);

        while (true) {
            String[] input = readInput();

            LocalDate start;
            LocalDate end;
            Currency currency;

            try {
                currency = Currency.valueOf(input[0]);
                start = LocalDate.parse(input[1]);
                end = LocalDate.parse(input[2]);

                if (end.isBefore(start)) {
                    System.out.println("Start date cannot be after end date! Try again!");
                    continue;
                }
            } catch (Exception e) {
                printInvalidArgumentError();
                continue;
            }

            try {
                FinanceStatistic fs = tracker.getFinanceStatistic(start, end, currency);
                printResult(fs);
                //TODO log exceptions
            } catch (NoNbpAccess e) {
                printNoAccessInfo();
            } catch (InvalidNbpData e) {
                printInvalidDate();
            } catch (HistoryLogWriteException e) {
                System.err.println("Error starting application: " + e.getCause().getMessage());
            } catch (Exception e) {
                printGeneralError();
            }
        }
    }

    private static void printInvalidDate() {
        System.out.println("Problem with reading data from NBP. Contact administrator!");
    }

    private static void printNoAccessInfo() {
        System.out.println("Problem with connecting to NBP. Try again later!");
    }

    private static String getFilePathForHistoryLog(String[] a) {
        if (a.length < 1)
            return DEFAULT_PATH;
        return a[0];
    }

    private static String[] readInput() {
        return SCANNER.nextLine().split(" ");
    }

    private static void printGeneralError() {
        System.out.println("Something went wrong!");
    }

    private static void printResult(FinanceStatistic fs) {
        System.out.println("Currency: " + fs.currency());
        System.out.println("Buying rates mean: " + fs.getBuyingRatesMean());
        System.out.println("Selling rates SD: " + fs.getSellingRatesStandardDeviation());
    }

    private static void printInvalidArgumentError() {
        System.out.println("Invalid input! Try again!");
    }


}

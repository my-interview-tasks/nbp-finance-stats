package com.pl.dlesiak.interview.rates.cache;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
class MemoryCacheTest {

    private Cache<Integer, String> cache = new MemoryCache<>();

    @Test
    void shouldAddToCache() {
        cache.put(1, "VALUE_1");

        String value = cache.get(1);

        assertEquals("VALUE_1", value);
    }

    @Test
    void shouldUpdateCache() {
        cacheAlreadyHasValueForKey();

        cache.put(1, "VALUE_2");

        String value = cache.get(1);
        assertEquals("VALUE_2", value);

    }

    private void cacheAlreadyHasValueForKey() {
        cache.put(1, "VALUE_1");
    }

}
package com.pl.dlesiak.interview.rates.history;

import com.pl.dlesiak.interview.rates.FinanceStatistic;

import java.io.IOException;

public class StandardOutputLog implements HistoryLog {

    @Override
    public void log(FinanceStatistic financeStatistic) {
        System.out.println(financeStatistic);
    }

    @Override
    public void close() throws IOException {

    }
}

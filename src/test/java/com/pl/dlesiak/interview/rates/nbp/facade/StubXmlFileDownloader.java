package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.downloader.Downloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

class StubXmlFileDownloader implements Downloader {

    @Override
    public InputStream download(String url) throws IOException {
        File initialFile = new File("src/test/resources/xml/c004z190107.xml");
        return new FileInputStream(initialFile);
    }
}

package com.pl.dlesiak.interview.rates.nbp.facade;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Optional;

final class NbpXmlFilenames {
    private final int year;
    private final Collection<String> xmlFileNames;

    NbpXmlFilenames(int year, Collection<String> xmlFileNames) {
        this.year = year;
        this.xmlFileNames = xmlFileNames;
    }

    int getYear() {
        return year;
    }

    Optional<String> getFilenameByDateAndType(LocalDate date, TableType tableType) {
        String shortDate = dateAsShortString(date);
        return xmlFileNames
                .stream()
                .filter(line -> line.contains(shortDate))
                .filter(line -> line.contains(tableType.getType()))
                .findAny();
    }

    private String dateAsShortString(LocalDate date) {
        String lastTwoDigitOfYear = (date.getYear() + "").substring(2);
        String monthWithPrecedingZero = date.getMonthValue() >= 10 ? date.getMonth().getValue() + "" : "0" + date.getMonthValue() + "";
        String dayWithPrecedingZero = date.getDayOfMonth() >= 10 ? date.getDayOfMonth() + "" : "0" + date.getDayOfMonth() + "";
        return lastTwoDigitOfYear + monthWithPrecedingZero + dayWithPrecedingZero;
    }

}

package com.pl.dlesiak.interview.rates.nbp.facade;

import com.pl.dlesiak.interview.rates.nbp.facade.exception.InvalidNbpData;
import com.pl.dlesiak.interview.rates.nbp.facade.model.NbpSellBuyRates;
import com.pl.dlesiak.interview.rates.nbp.facade.model.SellBuyNbpCurrencyRate;
import com.pl.dlesiak.interview.rates.rates.Currency;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashSet;

class NbpRateXmlParser {
    private final static String PUBLICATION_DATE = "data_publikacji";
    private final static String RECORD_DATE = "data_notowania";
    private final static String CURRENCY_CODE = "kod_waluty";
    private final static String CURRENCY_NAME = "nazwa_waluty";
    private final static String BUY_RATE_TAG = "kurs_kupna";
    private final static String SELL_RATE_TAG = "kurs_sprzedazy";
    private final static String POSITION_TAG = "pozycja";

    NbpSellBuyRates parseRates(Document document) {
        LocalDate publicationDate = extractDate(document, PUBLICATION_DATE);
        LocalDate recordDate = extractDate(document, RECORD_DATE);
        Collection<SellBuyNbpCurrencyRate> rates = extractPositions(document);
        return new NbpSellBuyRates(recordDate, publicationDate, rates);
    }

    private LocalDate extractDate(Document document, String dateTag) {
        return LocalDate.parse(getChildNode(dateTag, document.getDocumentElement()).getTextContent());
    }

    private Collection<SellBuyNbpCurrencyRate> extractPositions(Document document) {
        NodeList currencyRateTags = document.getElementsByTagName(POSITION_TAG);
        Collection<SellBuyNbpCurrencyRate> rates = new HashSet<>();

        for (var i = 0; i < currencyRateTags.getLength(); i++) {
            Node currencyRateTag = currencyRateTags.item(i);
            String currencyName = getChildNode(CURRENCY_NAME, currencyRateTag).getTextContent();
            Currency currency = Currency.valueOf(getChildNode(CURRENCY_CODE, currencyRateTag).getTextContent());
            double buyRate = getValue(currencyRateTag, BUY_RATE_TAG);
            double sellRate = getValue(currencyRateTag, SELL_RATE_TAG);
            rates.add(new SellBuyNbpCurrencyRate(currencyName, currency, buyRate, sellRate));
        }

        return rates;
    }

    private Node getChildNode(String tagName, Node node) {
        NodeList childNodes = node.getChildNodes();
        Node result = findTagInNode(tagName, childNodes);
        if (result == null) {
            throw new InvalidNbpData("No tag with name " + tagName + " found!");
        }
        return result;
    }

    private Node findTagInNode(String tagName, NodeList childNodes) {
        Node result = null;
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node child = childNodes.item(i);
            if (child instanceof Element) {
                Element element = (Element) child;

                if (tagName.equals(element.getTagName())) {
                    result = child;
                    break;
                }
            }
        }
        return result;
    }

    private double getValue(Node positionNode, String buyRateTag) {
        String textValue = getChildNode(buyRateTag, positionNode).getTextContent();
        return Double.parseDouble(textValue.replace(",", "."));
    }

}

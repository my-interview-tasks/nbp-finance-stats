package com.pl.dlesiak.interview.rates.rates;

import java.time.LocalDate;
import java.util.Objects;

public final class Rate {

    private final LocalDate timestamp;
    private final Currency currency;
    private final double buyingRate;
    private final double sellingRate;

    public Rate(LocalDate timestamp, Currency currency, double buyingRate, double sellingRate) {
        this.timestamp = timestamp;
        this.currency = currency;
        this.buyingRate = buyingRate;
        this.sellingRate = sellingRate;
    }

    public LocalDate getTimestamp() {
        return timestamp;
    }

    public double getBuyingRate() {
        return buyingRate;
    }

    public double getSellingRate() {
        return sellingRate;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rate rates = (Rate) o;
        return Objects.equals(timestamp, rates.timestamp) &&
                currency == rates.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, currency);
    }

    @Override
    public String toString() {
        return "Rates{" +
                "timestamp=" + timestamp +
                ", currency=" + currency +
                ", buyingRate=" + buyingRate +
                ", sellingRate=" + sellingRate +
                '}';
    }
}

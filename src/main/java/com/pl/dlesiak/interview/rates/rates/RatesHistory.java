package com.pl.dlesiak.interview.rates.rates;

import java.time.LocalDate;
import java.util.Collection;

public interface RatesHistory {

    Collection<Rate> getRates(LocalDate start, LocalDate end, Currency... currencies);


}

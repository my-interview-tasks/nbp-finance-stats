package com.pl.dlesiak.interview.rates.cache;

public interface Cache<KEY, VALUE> {

    VALUE put(KEY key, VALUE value);

    VALUE get(KEY key);

}
